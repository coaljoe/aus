package main

import (
//. "math"
//. "azul3d.org/lmath.v1"
//  . "bitbucket.org/coaljoe/lib/ecs"
)

type CombatHostI interface {
	getView() ViewI
	getObjType() string
	explode()
}

// component
type Combat struct {
	host    CombatHostI
	health  *Health // link
	//fireRate float64
	weapon1 *Weapon
	weapon2 *Weapon
}

func newCombat(host CombatHostI, health *Health) *Combat {
	cc := &Combat{
		host: host,
		health: health,
		//fireRate: 1.0,
	}
	cc.weapon1 = newWeapon(cc)
	cc.weapon2 = newWeapon(cc)
	return cc
}

func (c *Combat) takeDamage(amt int) {
	h := c.health
	h.takeHealth(amt)
	//v := h.health() - amt
	//h.setHealth(v)
	if !h.isAlive() {
		c.host.explode()
		//c.host.destroy()
		//panic("fixme: not implemented")
		//pub(ev_entity_destroy, GetEntity(c))
	}
}

func (c *Combat) destroy() {
	c.weapon1.destroy()
	c.weapon2.destroy()
}

func (cc *Combat) update(dt float64) {
	cc.weapon1.update(dt)
	cc.weapon2.update(dt)
}
