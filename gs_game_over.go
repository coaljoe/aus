package main

import (
	ps "bitbucket.org/coaljoe/lib/pubsub"
	"bitbucket.org/coaljoe/rx"
	"github.com/go-gl/glfw/v3.1/glfw"
)

type GameOverGameState struct {
	*GameState
}

func newGameOverGameState() *GameOverGameState {
	gs := &GameOverGameState{
		GameState: newGameState("game over"),
	}
	return gs
}

func (gs *GameOverGameState) onKeyPress(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	//pp(key)
	if key == glfw.KeyEnter || key == glfw.KeyEscape {
		setGameState("menu")
	}
}

func (gs *GameOverGameState) start() {
	g := game.gui
	g.rgi.SheetSys.SetActiveSheet(g.gameOverSheet)

	/*
		_Keymap.addKey(KeymapRec{Name: "enterKey", Key1: glfw.KeyEnter,
			Description:     "Enter key",
			DescriptionLong: ""})
	*/
	//playMusic("res/music/t2a-v3a.ogg")
	//stopMusic()
	sub(rx.Ev_key_press, gs.onKeyPress)
}

func (gs *GameOverGameState) stop() {
	g := game.gui
	g.rgi.SheetSys.SetActiveSheet(g.mainSheet)

	//_Keymap.removeKey("enterKey")
	//stopMusic()
	unsub(rx.Ev_key_press, gs.onKeyPress)
}

func (gs *GameOverGameState) update(dt float64) {

}
