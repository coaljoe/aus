package main

import (
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"
)

type BackgroundTile struct {
	meshes       []*rx.Node
	sizeX, sizeY float64
	prevPos, pos Vec3
}

func newBackgroundTile() *BackgroundTile {
	bt := &BackgroundTile{
		meshes: make([]*rx.Node, 0),
		sizeX:  64, sizeY: 64,
	}
	return bt
}

func (bt *BackgroundTile) placeMeshTimes(times int, path string) {
	m := rx.NewModel()
	m.Load(path)
	for i := 0; i < times; i++ {
		//meshNode := rx.NewNode(rx.NodeType_Mesh)
		//meshNode.Mesh.l
		m2 := m.Clone()
		nodes := m2.Spawn()
		px := random(0, bt.sizeX)
		py := random(0, bt.sizeY)
		for _, n := range nodes {
			n.SetPosZ(-100)
			n.Translate(Vec3{px, py, 0})
			bt.meshes = append(bt.meshes, n)
		}
	}
}

func (bt *BackgroundTile) placeMesh(path string) {
	m := rx.NewModel()
	m.Load(path)
	nodes := m.Spawn()
	for _, n := range nodes {
		n.SetPosZ(-100)
		bt.meshes = append(bt.meshes, n)
	}
}

func (bt *BackgroundTile) update(dt float64) {
	tX := bt.pos.X() - bt.prevPos.X()
	tY := bt.pos.Y() - bt.prevPos.Y()
	p("tY:", tY)
	for _, n := range bt.meshes {
		n.Translate(Vec3{tX, tY, 0})
		//n.Translate(Vec3{0, 10, 0})
		//n.MoveByVec(Vec3{0, 10, 0})
	}
	bt.prevPos = bt.pos
}

type Background struct {
	scrollSpeed float64
	tiles       []*BackgroundTile
	shY         float64
}

func newBackground() *Background {
	b := &Background{
		//scrollSpeed: 0.1,
		scrollSpeed: 5.0,
		//scrollSpeed: 100,
	}
	//b.addTiles(1)
	return b
}

func (b *Background) addTiles(n int) {
	for i := 0; i < n; i++ {
		tile := newBackgroundTile()

		b.tiles = append(b.tiles, tile)
		// Shift tile
		tile.pos.SetY(tile.sizeY * float64(len(b.tiles)-1))
	}
}

func (b *Background) update(dt float64) {
	b.shY = b.shY - b.scrollSpeed*dt
	//b.tiles[0].pos.SetY(b.shY)
	p("b.shY:", b.shY)
	for _, t := range b.tiles {
		posY := t.pos.Y()
		p("posY:", posY)
		//t.pos.SetY(posY + b.shY)
		t.pos.SetY(posY - b.scrollSpeed*dt) // XXX fixme: bug?
	}
	//pz("POS:", b.tiles[0].pos)
	for _, t := range b.tiles {
		t.update(dt)
	}
}
