package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"strconv"
	"strings"

	"bitbucket.org/coaljoe/lib/xlog"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"

	"github.com/kardianos/osext"
)

var _ = reflect.Bool
var _ = Vec3Zero

func init() {
	var runUnderTest = false
	if flag.Lookup("test.v") == nil {
		fmt.Println("normal run")
	} else {
		fmt.Println("run under go test")
		runUnderTest = true
	}

	// Change current dir to main's dir
	// Cd to app's root
	//dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	dir, err := osext.ExecutableFolder()
	if err != nil {
		panic(err)
	}
	fmt.Println("dir:", dir)
	//appRoot := filepath.Join(dir, "..")
	appRoot := dir
	if runUnderTest {
		wd, _ := os.Getwd()
		appRoot = wd // S/ltest
		//println("derp1", appRoot)
		appRoot = filepath.Join(appRoot, "..", "..")
		//println("derp2", appRoot)
		//println("wd", wd)
		//panic(2)
	}
	fmt.Println("appRoot: ", appRoot)
	os.Chdir(appRoot)

	// Set GOMAXPROCS.
	runtime.GOMAXPROCS(1)
	p("GOMAXPROCS:", runtime.GOMAXPROCS(0))
}

func main() {
	fmt.Println("main()")
	fmt.Printf("BuildRevision: %s", BuildRevision)

	ausMain()
}

func ausMain() {
	fmt.Println("main()")
	initApp()
	ok := initAudio()
	if !ok {
		panic("cannot init audio")
	}
	game, app := initBaseGame()
	initDefaultGame()

	rxi := rx.Rxi()
	//rxi.SetResPath("s/rx/res/")
	_ = rxi

	//rx.ConfSetDebugDraw(true)
	rx.ConfSetDebugDraw(false)
	rx.Conf.DisableExitOnEscape = true

	sl := rx.NewSceneLoader()
	_ = sl

	// Setup scene //

	c := _Scene.camera
	//c.setNamedCamera("blender1")
	c.setNamedCamera("topdown")

	//p(_Keymap.saveToJson())
	//s := _Keymap.saveToJson()
	//p(s)
	//_Keymap.loadFromJson(s)
	//pp(2)

	// Move camera target to 0, 0
	//c.MoveTargetTo(Vec3{0, c.trg.Y, 0})

	//

	// Load tank //

	/*
	  sl.Load("s/rx/res/test/tank_renew2/tank-renew2.dae")
	  sl.Spawn()
	  //rxi.Scene().GetNode("body").SetPos(Vec3{0, -5, 0})
	  //rxi.Scene().GetNode("turret").SetPos(Vec3{0, -5, 0})
	  rxi.Scene().GetNode("body").SetPos(Vec3{5, 0, 0})
	  rxi.Scene().GetNode("turret").SetPos(Vec3{5, 0, 0})
	  rxi.Scene().GetNode("turret").SetRot(Vec3{0, 45, 0})
	*/

	//path := "res/enemies/enemy1/enemy1.dae"
	prevConfValue := rx.Conf.DefaultMaterialPath
	rx.Conf.DefaultMaterialPath = "/materials/model.json"
	path := "res/objects/tree/tree.dae"
	m := rx.NewModel()
	m.Load(path)
	m.Spawn()
	rx.Conf.DefaultMaterialPath = prevConfValue
	//node := m.GetNode("body")
	node := m.GetNode("tree")
	node.SetPos(Vec3{0, 10, -100})
	//node.SetRot(Vec3{-30, -5, 2})
	mat := rx.NewMaterial("res/materials/model.json")
	_ = mat
	//node.Mesh.SetMaterial(mat)

	// GameStates

	initGameStates()
	//gameStates["main"].start()
	//gameStates["menu"].start()
	if vars.playLevel == "" {
		vars.playLevel = "level1"
		setGameState("menu")
	} else {
		//setGameState("menu")
		//setGameState("game")
		//setGameState("game over")
		//game.stop()
		setGameState("game")
		//game.playLevel("level2")
		//game.playLevel(vars.playLevel)
	}

	fps_limit := 0 //30

	for app.Step() {
		//println("main.step")
		dt := app.GetDt()
		game.update(dt)

		//p(e1.Pos(), e1.view.getNode("").Pos())

		if fps_limit != 0 {
			//ms_to_sleep := int64((1e6 / int64(fps_limit)) - app.GetDtMs())
			//time.Sleep(time.Duration(ms_to_sleep) * time.Millisecond)
			s_to_sleep := (1.0 / float64(fps_limit)) - dt
			p(s_to_sleep)
			if s_to_sleep > 0.0 {
				sleepsec(s_to_sleep / 2)
			}
			//sleepsec(0.5)
		}

		//sleepsec(0.1)
	}

	deinitAudio()
	println("exiting...")
}

/////////
// Util

// Set new window site.
func resizeWindow(sx, sy int) {
	_log.Inf("Resize window:", sx, sy)
	vars.resX = sx
	vars.resY = sy
	vars.resScaleX = float64(vars.resX) / float64(vars.nativeResX)
	vars.resScaleY = float64(vars.resY) / float64(vars.nativeResY)
}

/////////
// Init

var (
	_log        *xlog.Logger
	overrideRes bool // FIXME: add normal app module
	fullscreen  bool
)

func initApp() {
	println("Init() begin")
	SetDefaultVars()

	var optAppRoot = flag.String("root", "", "appRoot")
	var optRes = flag.String("res", "", "resolution WxH")
	var optFs = flag.Bool("fs", false, "full screen")
	var optDisableMusic = flag.Bool("disable_music", false, "disable music")
	var optDisableSound = flag.Bool("disable_sound", false, "disable sound")
	var optLevel = flag.String("level", "", "play level")

	flag.Parse()
	fmt.Println("args:", os.Args)
	//pp(2)
	if *optAppRoot != "" {
		//pp(*optAppRoot)
		appRoot := *optAppRoot
		fmt.Println("new appRoot:", appRoot)
		os.Chdir(appRoot)
	}
	if *optRes != "" {
		ss := strings.Split(*optRes, "x")
		sx, _ := strconv.Atoi(ss[0])
		sy, _ := strconv.Atoi(ss[1])
		fmt.Printf("new res: (%d, %d)\n", sx, sy)
		//resizeWindow(sx, sy)
		vars.resX = sx
		vars.resY = sy
		overrideRes = true
	}
	if *optFs == true {
		// XXX: comment this out to get -res option working with the -fs
		sx, sy := rx.GetDesktopResolution()
		vars.resX = sx
		vars.resY = sy

		overrideRes = true
		fullscreen = true
	}
	if *optDisableMusic == true {
		conf.disableMusic = true
	}
	if *optDisableSound == true {
		conf.disableSound = true
	}
	if *optLevel != "" {
		vars.playLevel = *optLevel
	}

	// Seed
	//rand.Seed(time.Now().UTC().UnixNano())

	// Create logger
	_log = xlog.NewLogger("aus", xlog.LogDebug)
	if loglev := os.Getenv("LOGLEVEL"); loglev != "" {
		_log.SetLogLevelStr(loglev)
	}
	_log.SetOutputFile("aus.log")
	_log.SetWriteTime(true)
	println("Init() done")
}

func initBaseGame() (*Game, *rx.App) {
	println("InitGame() begin")

	// Load settings
	defaultSettingsFile := strings.ToLower(AppName) + "_default.json"
	customSettingsFile := strings.ToLower(AppName) + ".json"

	// Pick default or custom settings file
	fp := ""
	if found, err := exists(customSettingsFile); found {
		if err != nil {
			panic(err)
		}
		fp = customSettingsFile
	} else if found, err = exists(defaultSettingsFile); found {
		if err != nil {
			panic(err)
		}
		fp = defaultSettingsFile
	} else {
		pp("Settings file not found in:", defaultSettingsFile, customSettingsFile)
	}

	// Load settings
	s := newSettings()
	s.load(fp)
	s.verify()
	//pdump(s)

	// Apply settings
	if !overrideRes {
		vars.resX = s.getInt("resX")
		vars.resY = s.getInt("resY")
	}

	// Set config
	rx.SetDefaultConf()
	//rx.ConfSetResPath("/home/j/dev/go/s/rx/res/") // Fixme
	rx.ConfSetResPath("res/") // Fixme
	// Set extra conf
	rx.Conf.EnableCulling = false
	//rx.Conf.EnableCulling = true

	//rx.Init()
	//win := rx.NewWindow()
	//win.Init(640, 480)
	//win.Init(1024, 576)
	//rxi := rx.Init(1024, 576)
	var rxi *rx.Rx
	if !fullscreen {
		rxi = rx.Init(vars.resX, vars.resY)
	} else {
		rxi = rx.InitFullscreen(vars.resX, vars.resY)
	}
	app := rxi.App
	game := newGame(app)
	// Set debug game options
	game.setGameOptions(newDebugGameOptions())
	println("InitGame() done")

	app.Win().SetTitle(AppName + "  (rev. " + BuildRevision + ")")

	// Update globals
	_rxi = rxi

	// Finally
	//s.save("test_settings_out.json")

	return game, app
}

func initDefaultGame() {
	//game.createNewDefaultGame()
	game.start()
}
