package main

import (
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"

	"github.com/go-gl/gl/v2.1/gl"
)

type HudView struct {
	m *Hud
}

func newHudView(h *Hud) *HudView {
	hv := &HudView{m: h}

	// XXX: temporary, for testing only
	rx.Rxi().Renderer().SetPostRenderCB(hv.render)
	return hv
}

// Draw selection box for the object.
func (v *HudView) drawSelectionFrame(o *Obj) {
	frameSizePxW := 26.0
	frameSizePxH := 26.0
	frameColor := Vec3{.92, .94, .96}
	lifebarColor := Vec3{0.1, 0.7, 0.1}

	r := rx.Rxi().Renderer()
	//r.Set2DMode()
	pos := o.Pos()
	sxi, syi := r.Size()
	sx, sy := float64(sxi), float64(syi)
	pw, ph := r.GetPw(), r.GetPh()
	cam := rx.Rxi().Camera.Camera
	screen_point := Project(pos, cam.GetViewMatrix(), cam.GetProjectionMatrix(), 0, 0, sxi, syi)
	//p("screen_point:", screen_point)
	p_sx, p_sy := screen_point.X()/sx, screen_point.Y()/sy
	camDefZoom := 23.0 //game.scene.camera.defaultZoom // XXX fixme
	camCurZoom := game.scene.camera.cam.Camera.Zoom()
	zoom := camDefZoom / camCurZoom
	//p("camDefZoom:", camDefZoom, "camCuzZoom:", camCurZoom, "zoom:", zoom)
	fw := frameSizePxW * pw * zoom * vars.resScaleX
	fh := frameSizePxH * ph * zoom * vars.resScaleY
	coords := Vec4{p_sx - fw/2, p_sy - fh/2, fw, fh}
	//p("coords:", coords)
	//gl.PushMatrix()
	//gl.Translatef(0, 0, 1)
	gl.Disable(gl.DEPTH_TEST)
	gl.Enable(gl.LINE_SMOOTH)

	// Draw box
	rx.DrawSetLineWidth(1)
	//rx.DrawSetColor(.92, .94, .96)
	rx.DrawSetColorV(frameColor)
	rx.DrawRectV(coords)

	// Draw lifebar
	ox := 2.0 * pw * zoom
	oy := 2.0 * ph * zoom
	p0 := Vec3{p_sx - fw/2 + ox, p_sy + fh/2 - oy, 0}
	p1 := Vec3{p_sx + fw/2 - ox, p_sy + fh/2 - oy, 0}
	//rx.DrawSetColor(0.1, 0.7, 0.1)
	rx.DrawSetColorV(lifebarColor)
	rx.DrawSetLineWidth(2)
	rx.DrawLineV(p0, p1)

	gl.Enable(gl.LINE_SMOOTH)
	gl.Enable(gl.DEPTH_TEST)
	//gl.PopMatrix()
	rx.DrawResetColor()
	//r.Unset2DMode()
}

func (v *HudView) render(r *rx.Renderer) {
	m := v.m

	r.Set2DMode()

	//pp("derp")

	//r.Set2DMode()
	//gl.PushMatrix()
	//rx.DrawLine(0, 0, 1, 10, 10, 1)
	//p("draw line")

	//rx.DrawLine(0, 0, 0, .5, .5, 0)
	//gl.PopMatrix()
	r.Unset2DMode()

	// Debug

	if m.debug {
	}
}

func (v *HudView) update(dt float64) {
	m := v.m
	_ = m

	// XXX can't execute draw calls here
	var _ = `
	r := rx.Rxi().Renderer()

	if m.selecting {
		r.Set2DMode()
		//gl.PushMatrix()
		sxi, syi := rx.Rxi().Renderer().Size()
		sx, sy := float64(sxi), float64(syi)
		_r := m.selRect
		x1, y1 := _r[0] / sx, _r[1] / sy
		x2, y2 := _r[2] / sx, _r[2] / sy
		z := 1.
		rx.DrawLine(_r[0], _r[1], 0, _r[2], _r[3], 0)
		//p("bitbucket.org/coaljoe/rx.DrawLine:", _r[0], _r[1], 0, _r[2], _r[3], 0)
		rx.DrawLine(x1, y1, z, x2, y2, z)
		p("bitbucket.org/coaljoe/rx.DrawLine:", x1, y1, 0, x2, y2, 0)
		//gl.PopMatrix()
		r.Unset2DMode()
	}

	r.Set2DMode()
	//gl.PushMatrix()
	rx.DrawLine(0, 0, 1, 10, 10, 1)
	p("draw line")

	rx.DrawLine(0, 0, 0, .5, .5, 0)
	//gl.PopMatrix()
	r.Unset2DMode()
	`
}
