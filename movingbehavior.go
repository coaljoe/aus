package main

import (
	"math/rand"
	. "bitbucket.org/coaljoe/rx/math"
)

type MovingBhvI interface {
	start()
	update(dt float64)
}

type BaseMovingBhv struct {
	name string
}

func newBaseMovingBhv(name string) *BaseMovingBhv {
	bmb := &BaseMovingBhv{
		name: name,
	}
	return bmb
}

/***** Standard Moving Behavior *****/

type StandardMovingBhv struct {
	*BaseMovingBhv
	moveSpeed float64
	e         *Enemy
}

func newStandardMovingBhv(e *Enemy) *StandardMovingBhv {
	mb := &StandardMovingBhv{
		BaseMovingBhv: newBaseMovingBhv("Standard"),
		//moveSpeed:     14,
		moveSpeed: 12,
		e:         e,
	}

	return mb
}

func (mb *StandardMovingBhv) start() {
	e := mb.e
	//e.AdjPos(Vec3{0, gameWindowH, 0})
	e.position.dir.setAngle(-90)
	dPos := Vec3{e.Pos().X(), -10, 0}
	e.position.setDPos(dPos)
	//c.position.setDPos(Vec3Zero)
	e.position.speed = mb.moveSpeed
	//c.position.speed = 1
}

func (mb *StandardMovingBhv) update(dt float64) {
}

/***** Strafing Moving Behavior *****/

type StrafingMovingBhv struct {
	*BaseMovingBhv
	moveSpeed      float64
	strafingAmount float64
	e              *Enemy
}

func newStrafingMovingBhv(e *Enemy) *StrafingMovingBhv {
	mb := &StrafingMovingBhv{
		BaseMovingBhv: newBaseMovingBhv("Strafing"),
		moveSpeed:     20.0,
		//strafingAmount: 10.0,
		strafingAmount: 5.0,
		e:              e,
	}

	return mb
}

func (mb *StrafingMovingBhv) start() {
	e := mb.e
	//e.AdjPos(Vec3{0, gameWindowH, 0})
	e.position.dir.setAngle(-90)
	dPos := Vec3{e.Pos().X(), -10, 0}
	e.position.setDPos(dPos)
	//c.position.setDPos(Vec3Zero)
	e.position.speed = mb.moveSpeed
	//c.position.speed = 1
}

func (mb *StrafingMovingBhv) update(dt float64) {
	rx := rand.Float64() * mb.strafingAmount * dt
	mb.e.AdjPos(Vec3{rx, 0, 0})
}

/***** Strafing2 Moving Behavior *****/

type Strafing2MovingBhv struct {
	*BaseMovingBhv
	moveSpeed               float64
	strafingAmount          float64
	strafingTimer           *Timer
	strafingTimerSwitchTime float64
	strafingPosX            float64
	e                       *Enemy
}

func newStrafing2MovingBhv(e *Enemy) *Strafing2MovingBhv {
	mb := &Strafing2MovingBhv{
		BaseMovingBhv: newBaseMovingBhv("Strafing2"),
		moveSpeed:     12.0,
		//strafingAmount: 10.0,
		strafingAmount:          0.1,
		strafingTimer:           newTimer(false),
		strafingTimerSwitchTime: 5.0,
		e: e,
	}

	return mb
}

func (mb *Strafing2MovingBhv) start() {
	e := mb.e
	//e.AdjPos(Vec3{0, gameWindowH, 0})
	e.position.dir.setAngle(-90)
	dPos := Vec3{e.Pos().X(), -10, 0}
	e.position.setDPos(dPos)
	//c.position.setDPos(Vec3Zero)
	e.position.speed = mb.moveSpeed
	//c.position.speed = 1
	mb.strafingTimer.start()
}

func (mb *Strafing2MovingBhv) update(dt float64) {
	if mb.strafingTimer.dt() < mb.strafingTimerSwitchTime {
		mb.strafingPosX += mb.strafingAmount * dt
		rv := mb.strafingPosX
		mb.e.AdjPos(Vec3{rv, 0, 0})
	} else {
		mb.strafingTimer.restart()
	}
}
