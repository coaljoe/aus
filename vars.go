package main

import (
	"os"
	//. "bitbucket.org/coaljoe/rx/math"
)

const (
	Debug       = true
	AppName     = "aus"
	gameWindowW = 50.0
	gameWindowH = 100.0
)

var (
	BuildRevision string = "unknown"
)

var vars struct {
	scrollSpeed float64
	// Screen resolution
	resX int
	resY int
	// Native screen resolution for fixed-size elements
	nativeResX int
	nativeResY int
	// Resolution scale coeff. for fixed-size elements
	resScaleX float64
	resScaleY float64
	// Wrapped world space is used
	wrapped bool
	// For temporary files
	tmpDir    string
	gameSpeed float64
	// Time Delta
	dt float64
	// Game speed-independant Time Delta
	dtGameSpeed float64
	playLevel   string
}

func SetDefaultVars() {
	//vars.gameWindow = Vec4{0, 0, 10, 50} // x, y, w, h
	vars.scrollSpeed = 25.0
	//vars.scrollSpeedFast = 100.0
	vars.resX = 1024
	vars.resY = 576
	vars.nativeResX = 1024
	vars.nativeResY = 576
	vars.resScaleX = 1
	vars.resScaleY = 1
	vars.wrapped = false
	vars.gameSpeed = 1
	vars.dt = 0
	vars.dtGameSpeed = 0
	vars.playLevel = ""

	vars.tmpDir = os.TempDir()
}
