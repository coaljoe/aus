package main

import (
	. "bitbucket.org/coaljoe/rx/math"
)

func makeEnemy(name string) *Enemy {
	e := newEnemy()
	e.name = name
	if name == "enemy1" {
		//e.combat.weapon1.fireRate = 30
		e.combat.weapon1.fireRate = 15
		e.combat.weapon1.shotSpeed = 25
		//e.movingBhv = newStrafingMovingBhv(e)
		e.movingBhv = newStrafing2MovingBhv(e)
		return e
	} else if name == "enemy2" {
		e.color = Vec3{1.0, 0, 0}
		//e.combat.weapon1.fireRate = 80
		e.combat.weapon1.fireRate = 50
		e.movingBhv.(*StandardMovingBhv).moveSpeed = 18 // fixme?
		e.killScore = 150
	} else {
		pp("Unknown enemy name:", name)
	}

	return e
}

// helper
func makeEnemyAt(name string, pos Vec3) {
	e := makeEnemy(name)
	e.SetPos(pos)
	spawnEntity(e)
}
