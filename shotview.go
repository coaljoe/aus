package main

import (
	"bitbucket.org/coaljoe/rx"
	//. "bitbucket.org/coaljoe/rx/math"
	"fmt"
)

//var bulletModel *rx.Model

// Component
type ShotView struct {
	*View
	assetPath string
	m         *Shot
	nodeName  string
	node      *rx.Node
}

func newShotView(m *Shot) *ShotView {
	v := &ShotView{
		View:     newView(),
		m:        m,
		nodeName: "shot",
	}
	return v
}

func (v *ShotView) load() {
	_log.Dbg("ShotView.load()")

	/*
		// init resources
		if bulletModel == nil {
			n := rx.NewModel()
			bulletModel = n
			bulletModel.Load("res/objects/bullets/bullet/model.dae")
		}

		// clone resources
		clone := bulletModel.Clone()
		v.node = clone.GetNode(v.nodeName)
		//clone.PrintNodes()
		v.model = clone
	*/
	//path := "res/objects/bullets/bullet/model.dae"
	path := fmt.Sprintf("res/objects/shots/%s/%s.dae", v.m.name, v.m.name)
	if modelCache[path] == nil {
		n := rx.NewModel()
		modelCache[path] = n
		n.Load(path)
	}
	clone := modelCache[path].Clone()
	v.node = clone.GetNode(v.nodeName)
	v.model = clone
	//pp(2)
	/*
		v.asset.Load("res/objects/bullets/bullet/model.dae")
		v.node = v.asset.GetNode(v.nodeName)
	*/
	v.loaded = true
}

func (v *ShotView) spawn() {
	println("ShotView.spawn()")
	if !v.loaded {
		v.load()
	}
	//v.node.spawn()
	rx.Rxi().Scene().Add(v.node)
	// Orientation
	//v.node.LookAt2d(v.m.dPos)
	v.spawned = true
}

func (v *ShotView) destroy() {
	println("ShotView.destroy()")
	rx.Rxi().Scene().RemoveMeshNode(v.node)
	v.spawned = false
}

func (v *ShotView) update(dt float64) {
	newPos := v.m.Pos()
	//curPos := v.node.Pos()
	v.node.SetPos(newPos)
	v.node.SetRot(v.m.Rot())

	// Fixme: can be called once?

	/*
		if v.m.position.hasDPos() {
			v.node.LookAt2d(v.m.position.dPos())
		}
	*/

}
