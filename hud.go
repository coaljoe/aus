package main

import (
	ps "bitbucket.org/coaljoe/lib/pubsub"
	"bitbucket.org/coaljoe/rx"

	"github.com/go-gl/glfw/v3.1/glfw"
)

// Hud system. A part of Gui.
type Hud struct {
	*GameSystem
	mx, my  int
	cx, cy  int
	enabled bool
	view    *HudView
	debug   bool
	// Debug
	d_showMouseRay bool
}

func newHud() *Hud {
	h := &Hud{
		mx:             -1,
		my:             -1,
		debug:          true,
		d_showMouseRay: true,
	}
	h.GameSystem = newGameSystem("Hud", "Hud Subsystem", h)
	//sub(rx.Ev_mouse_move, h.onMouseMove)
	//sub(rx.Ev_mouse_press, h.onMousePress)
	//sub(rx.Ev_mouse_release, h.onMouseRelease)
	sub(rx.Ev_key_press, h.onKeyPress)
	return h
}

/*
func (h *Hud) onMouseMove(ev *ps.Event) {
	if game.gui.hasMouseFocus() {
		return
	}

	p := ev.Data.(rx.EvMouseMoveData)
	h.mx = p.X
	h.my = p.Y
	//pp(h.mx, h.my)
	_log.Trc("hud.onmousemove", h.mx, h.my)

	if rx.Rxi().InputSys.Mouse.IsButtonPressed(glfw.MouseButtonLeft) {
		if !h.selecting {
			h._startSelection()
		} else {
			h._updateSelection()
		}
	}

	//h._doGroundPlaneHitTest()
	if h.debug {
		//_Scene.debugBox.SetPos(h.groundPoint)
	}
}

func (h *Hud) onMousePress(ev *ps.Event) {
	if game.gui.hasMouseFocus() {
		return
	}

	if h.mx == -1 && h.my == -1 {
		h.mx = _InputSys.Mouse.X
		h.my = _InputSys.Mouse.Y
		//pp(h.mx, h.my)
	}
	btn := ev.Data.(glfw.MouseButton)
	_log.Dbg("hud.onmousepress", btn)
	h._doGroundPlaneHitTest()

	if btn == glfw.MouseButtonLeft {
		switch h.mouseState {
		case HudMouseState_Default:
			for _, o := range h.selObjs {
				if o.objtype == ObjType_Unit {
					// Move units
					c_Unit(GetEntity(o)).Mover.moveTo(CPos{h.cx, h.cy})
				}
				// Unselect
				h.RemoveObjFromSelection(o)
			}
		case HudMouseState_PlaceBuilding:
			// Place building
			btype := "bunker"
			h._placeBuilding(btype)

			// Store last placed building type
			h.lastBuildingType = btype

		default:
			pp("unknown mouseState:", h.mouseState)
		}

		// Update mouseState
		switch h.mouseState {
		case HudMouseState_PlaceBuilding:
			h.mouseState = HudMouseState_Default
			//default:
			//	pp("unknown mouseState:", h.mouseState)
		}
	} else if btn == glfw.MouseButtonRight {
		h.mouseState = HudMouseState_PlaceBuilding
	}
}

func (h *Hud) onMouseRelease(ev *ps.Event) {
	if game.gui.hasMouseFocus() {
		return
	}

	btn := ev.Data.(glfw.MouseButton)
	_log.Dbg("hud.onmouserelease", btn)
	if !h.selecting {
		//h._doSingleSelection()
	} else {
		h._stopSelection()
	}
}
*/
func (h *Hud) onKeyPress(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	if key == _Keymap.getKey1("rebuildLastBuildingKey") {
		pp("derp")
	}
}

func (h *Hud) start() {
	h.view = newHudView(h)
	h.enabled = true
	if h.debug {
		//_Scene.debugBox.SetVisible(true)
	}
}

func (h *Hud) update(dt float64) {
	if !h.enabled {
		return
	}
	//pp("derp")
	h.view.update(dt)

	// Pass the information to the gui
	//_Gui.cellX = h.cx
	//_Gui.cellY = h.cy
}
