package main

import (
	. "bitbucket.org/coaljoe/rx/math"
)

// entity
type Ship struct {
	*Obj
	health *Health
	combat *Combat
	// props
	//fireInterval float64
	immortal bool
}

func newShip() *Ship {
	s := &Ship{
		Obj:      newObj("ship"),
		health:   newHealth(),
		immortal: false,
		//fireInterval: 1,
	}
	s.colobj = newColObj(s.Obj, 3.5)
	s.combat = newCombat(s, s.health)
	s.view = newShipView(s)

	// Tune
	s.combat.weapon1.fireRate = 150
	s.combat.weapon1.shotSound = "res/audio/slimeball.wav"
	s.combat.weapon2.fireRate = 900
	s.combat.weapon2.shotName = "shot2"
	s.combat.weapon2.shotSound = "res/audio/iceball.wav"
	//s.combat.weapon2.shotSpeed = 200
	s.combat.weapon2.shotSpeed = 175
	return s
}

func (s *Ship) start() {
	s.SetRotZ(90.0)
	// Center the ship
	s.SetPosX(gameWindowW / 2)
	s.SetPosY(5) // FIXME: paddingH
}

// Crash the ship
func (s *Ship) explode() {
	playSound("res/audio/SFX_Explosion_08.wav", vol_sfx)
	//destroyEntity(s)
	tryDestroyEntity(s) // XXX fixme
	game.player.takeLives()
	if game.player.lives == 0 {
		go func() {
			//stopMusic()
			sleepsec(2)
			setGameState("game over")
		}()
	} else {
		// Respawn ship
		respawnCb := func() {
			game.ship = newShip()
			spawnEntity(game.ship)
			game.ship.immortal = true
		}
		//game.ship.start()
		disableImmortalCb := func() {
			game.ship.immortal = false
			game.ship.view.(*ShipView).show() // fixme?
		}
		_ = newShed(1.0, respawnCb)
		_ = newShed(4.0, disableImmortalCb)
	}
}

func (s *Ship) destroy() {
	s.Obj.destroy()
	s.health.destroy()
	s.combat.destroy()
}

func (e *Ship) update(dt float64) {
	kb := _InputSys.Keyboard
	velX := 0.0
	velY := 0.0
	moveSpeed := 6.0
	rot := 0.0
	rotAmt := 12.5
	if kb.IsKeyPressed(_Keymap.getKey1("moveUpKey")) {
		velY = moveSpeed
	}
	if kb.IsKeyPressed(_Keymap.getKey1("moveDownKey")) {
		velY = -moveSpeed
	}
	if kb.IsKeyPressed(_Keymap.getKey1("moveLeftKey")) {
		velX = -moveSpeed
		rot = -rotAmt
	}
	if kb.IsKeyPressed(_Keymap.getKey1("moveRightKey")) {
		velX = moveSpeed
		rot = rotAmt
	}
	_ = rot

	if velX != 0 || velY != 0 {
		p("velX:", velX, "velY:", velY)

		moveAdj := Vec3{velX, velY, 0}

		curPos := e.Pos()
		newPos := curPos.Add(moveAdj.MulScalar(moveSpeed * dt))

		// Limit the movement range
		padding := 3.5
		if newPos.X() < padding {
			newPos.SetX(padding)
		} else if newPos.X() > (gameWindowW - padding) {
			newPos.SetX(gameWindowW - padding)
		}
		paddingH := 4.5
		if newPos.Y() < paddingH {
			newPos.SetY(paddingH)
		} else if newPos.Y() > (gameWindowH - paddingH) {
			newPos.SetY(gameWindowH - paddingH)
		}
		e.SetPos(newPos)
		p(newPos)
	}

	// Rotate

	e.SetRotX(rot)

	// Update firing

	if kb.IsKeyPressed(_Keymap.getKey1("weapon1Key")) && !e.combat.weapon1.firing {
		p("firing weapon1")
		p(e.combat.weapon1.firing)
		e.combat.weapon1.rounds = 10
		e.combat.weapon1.startFiring()
		//playSoundChannel("test.wav", 1)
	} else {
		//p("stop firing weapon1")
		e.combat.weapon1.stopFiring()
	}

	if kb.IsKeyPressed(_Keymap.getKey1("weapon2Key")) && !e.combat.weapon2.firing {
		if game.player.weapon2Power > game.player.weapon2PowerShootCost {
			//if game.player.weapon2Power > 0.05 {
			p("firing weapon2")
			p(e.combat.weapon2.firing)
			e.combat.weapon2.rounds = 10
			e.combat.weapon2.startFiring()
			//playSoundChannel("test.wav", 1)
			game.player.takeWeapon2Power()
			game.player.weapon2BeepLock = false
		} else {
			game.player.weapon2Power = 0.0
			if game.player.weapon2BeepLock == false {
				//playSound("res/audio/TF_GUI-Sound-8.wav", vol_sfx)
				playSound("res/audio/weapon2_beep.wav", vol_max)
				game.player.weapon2BeepLock = true
			}
		}
	} else {
		//p("stop firing weapon1")
		e.combat.weapon2.stopFiring()
	}

	// Update components

	e.Obj.update(dt)
	e.combat.update(dt)
}
