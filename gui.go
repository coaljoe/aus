package main

import (
	//"bitbucket.org/coaljoe/rx"
	rg "bitbucket.org/coaljoe/rx/rxgui"
)

type Gui struct {
	rgi *rg.RxGui
	//sheet *rg.Sheet
	//topText   *rg.Label
	//debugText *rg.Label
	mainSheet     *GuiMainSheet
	menuSheet     *GuiMenuSheet
	gameOverSheet *GuiGameOverSheet
	enabled       bool
}

func newGui() *Gui {
	g := &Gui{
		rgi: rg.NewRxGui(),
		//sheet:   rg.NewSheet("GUI"),
		//mainSheet: newGuiMainSheet(),
		enabled: true,
		//enabled: true,
	}

	if true {
		g.mainSheet = newGuiMainSheet(g)
		//g.mainSheet.SetEnabled(true)
		g.menuSheet = newGuiMenuSheet(g)
		//g.menuSheet.SetEnabled(true)
		g.gameOverSheet = newGuiGameOverSheet(g)
		//g.rgi.SheetSys.SetActiveSheet(g.menuSheet)
		/*
			g.rgi.SheetSys.AddSheet(g.sheet)
			g.rgi.SetEnabled(g.enabled)

			g.topText = rg.NewLabel(rg.Pos{0, 0}, "topText")
			//g.sheet.AddWidget(g.topText)
			g.sheet.Root().AddChild(g.topText)

			g.debugText = rg.NewLabel(rg.Pos{0, 0.97}, "debugText")
			g.debugText.SetVisible(false)
			g.sheet.Root().AddChild(g.debugText)
		*/
		/*
			x := rg.NewRxGui()
			x.SetEnabled(true)
			g := &Gui{rgi: x}
		*/

		/*
			// Panel test
			//p := rg.NewPanel(rg.Rect{0, 0, 100, 100})
			//p := rg.NewPanel(rg.Rect{0, 0, 10, 10})
			//p := rg.NewPanel(rg.Rect{0, 0, .2, .2})
			p := rg.NewPanel(rg.Rect{.5, .5, .5, .5})
			p.SetName("blue panel")
			p.SetColor(&rg.ColorBlue)
			//mySheet.AddWidget(p)
			g.mainSheet.Root().AddChild(p)
		*/

	}

	return g
}

func (g *Gui) hasMouseFocus() bool {
	//return true
	return g.rgi.HasMouseFocus()
}

func (g *Gui) showDebugText(s string) {
	/*
		g.debugText.SetText(s)
		g.debugText.SetVisible(true)
	*/
	g.mainSheet.debugText.SetText(s)
	g.mainSheet.debugText.SetVisible(true)
}

func (g *Gui) hideDebugText() {
	/*
		g.debugText.SetText("")
		g.debugText.SetVisible(false)
	*/
	g.mainSheet.debugText.SetText("")
	g.mainSheet.debugText.SetVisible(false)
}

func (g *Gui) showTopText(s string) {
	g.mainSheet.topText.SetText(s)
	//g.mainSheet.topText.SetVisible(true)
	//g.mainSheet.Root().AddChild(g.mainSheet.topText)
}

func (g *Gui) hideTopText() {
	g.mainSheet.topText.SetText("")
	//g.mainSheet.topText.SetVisible(false)
}

func (g *Gui) popupTopText(s string, t float64) {
	g.showTopText(s)
	hidePopupCb := func() {
		g.hideTopText()
	}
	_ = newShed(t, hidePopupCb)
}

var _ = `
func (g *Gui) render(r *rx.Renderer) {
	if !g.enabled {
		return
	}
	//g.sheet.Render(r)
	g.mainSheet.Render(r)
	g.menuSheet.Render(r)
}

func (g *Gui) update(dt float64) {
	if !g.enabled {
		return
	}
	//pp(2)

	// XXX not needed? double update?
	//g.render(rx.Rxi().Renderer())
	//g.render(_rxi.Renderer())

	/*
		//g.topText.SetText(fmt.Sprintf("cellX: %d cellY: %d", g.cellX, g.cellY))
		g.topText.SetText(fmt.Sprintf("cellX: %d cellY: %d fps: %d",
			g.cellX, g.cellY, _rxi.App.Fps()))
	*/
	g.mainSheet.Update(dt)
	g.menuSheet.Update(dt)
}
`
