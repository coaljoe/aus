package main

import (
	//. "math"
	"bitbucket.org/coaljoe/rx"
	//. "bitbucket.org/coaljoe/rx/math"
	rg "bitbucket.org/coaljoe/rx/rxgui"
)

type GuiGameOverSheet struct {
	*rg.Sheet
	topText    *rg.Label
	debugText  *rg.Label
	statusText *rg.Label
}

func newGuiGameOverSheet(g *Gui) *GuiGameOverSheet {
	s := &GuiGameOverSheet{
		Sheet: rg.NewSheet("Game over sheet"),
	}

	g.rgi.SheetSys.AddSheet(s)
	s.topText = rg.NewLabel(rg.Pos{0.4, 0.5}, "Game Over")
	s.topText.SetVisible(false)
	s.topText.SetBgColor(rg.ColorBlue)
	s.Root().AddChild(s.topText)

	return s
}

func (s *GuiGameOverSheet) Render(r *rx.Renderer) {
	// Call base
	s.Sheet.Render(r)
}

func (s *GuiGameOverSheet) Update(dt float64) {
	// Call base
	s.Sheet.Update(dt)
}
