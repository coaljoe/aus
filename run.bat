@echo off
setlocal
@rem call env.bat
@rem for /f "delims=" %%i in ('hg id -i') do set _rev=%%i
set PATH=%PATH%;bin

@rem always clean
@rem go install -v rx rx/math lib/ecs lib/xlog lib/sr lib/pubsub 
del aus.exe 2> nul

@rem go run -v src\main.go
@rem go install -v -ldflags="-X 'main.BuildRevision=%_rev%'" ltest 
go build

:run
aus.exe
goto end

:end
endlocal
