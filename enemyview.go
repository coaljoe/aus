package main

import (
	"fmt"
	//. "math"
	. "bitbucket.org/coaljoe/rx/math"
	//  . "bitbucket.org/coaljoe/lib/ecs"
	"bitbucket.org/coaljoe/rx"
)

//var enemyModel *rx.Model

type EnemyView struct {
	*View
	m    *Enemy
	node *rx.Node
}

func newEnemyView(m *Enemy) *EnemyView {
	v := &EnemyView{
		View: newView(),
		m:    m,
	}
	return v
}

func (v *EnemyView) load() {
	/*
		if enemyModel == nil  {
			n := rx.NewModel()
			enemyModel = n
			path := fmt.Sprintf("res/enemies/%s/%s.dae", v.m.name, v.m.name)
			enemyModel.Load(path)
		clone := enemyModel.Clone()
		v.node = clone.GetNode("body")
		}
	*/
	path := fmt.Sprintf("res/enemies/%s/%s.dae", v.m.name, v.m.name)
	if modelCache[path] == nil {
		n := rx.NewModel()
		modelCache[path] = n
		n.Load(path)
	}
	clone := modelCache[path].Clone()
	v.node = clone.GetNode("body")
	v.model = clone

	if v.node == nil {
		panic("no v.node")
	}
	v.loaded = true
}

func (v *EnemyView) spawn() {
	if !v.loaded {
		v.load()
	}
	//v.node.SetRot(Vec3{90, 0, -90})
	//v.node.SetRot(Vec3{0, 0, -90})
	rx.Rxi().Scene().Add(v.node)
	//v.spawned = true
	//pp(v.node.Material().Diffuse)
}

func (v *EnemyView) destroy() {
	rx.Rxi().Scene().RemoveMeshNode(v.node)
}

func (v *EnemyView) update(dt float64) {
	// XXX fixme: temporary bugfix
	if v.m == nil {
		return
	}
	v.node.SetPos(v.m.Pos())
	v.node.SetRot(Vec3{0, 0, v.m.position.dir.angle()})
}
