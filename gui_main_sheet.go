package main

import (
	"fmt"
	//. "math"
	"bitbucket.org/coaljoe/rx"
	//. "bitbucket.org/coaljoe/rx/math"
	"github.com/go-gl/gl/v2.1/gl"
	rg "bitbucket.org/coaljoe/rx/rxgui"
)

// fixme: rename to GuiGameSheet?
type GuiMainSheet struct {
	*rg.Sheet
	topText    *rg.Label
	debugText  *rg.Label
	statusText *rg.Label
}

func newGuiMainSheet(g *Gui) *GuiMainSheet {
	s := &GuiMainSheet{
		Sheet: rg.NewSheet("MySheet"),
	}
	//g := _Gui
	g.rgi.SheetSys.AddSheet(s)
	//g.rgi.SetEnabled(g.enabled)

	//s.topText = rg.NewLabel(rg.Pos{0.4, 0.5}, "topText")
	s.topText = rg.NewLabel(rg.Pos{0.4, 0.5}, "")
	//g.sheet.AddWidget(g.topText)
	s.topText.SetVisible(false)
	//s.topText.SetVisible(true) // XXX fixme: adds panel-like border to label
	s.Root().AddChild(s.topText)

	//s.debugText = rg.NewLabel(rg.Pos{0, 0.97}, "debugText")
	s.debugText = rg.NewLabel(rg.Pos{0.34, 0.97}, "debugText")
	s.debugText.SetVisible(false)
	s.Root().AddChild(s.debugText)
	//color := rg.ColorRed
	//s.debugText.SetBgColor(&rg.ColorBlue)
	//pp(s.debugText.BgColor())

	s.statusText = rg.NewLabel(rg.Pos{0, 0.97}, "statusText")
	s.statusText.SetVisible(false)
	s.Root().AddChild(s.statusText)

	return s
}

func (s *GuiMainSheet) Render(r *rx.Renderer) {
	// Call base
	s.Sheet.Render(r)

	//println("MYSHEET RENDER")
	//r := rxi.Renderer()

	//r.RenderQuad(0.0, 0.97, 0.2, 0.1, 10000)
	//rx.DrawSetColor(1.0, 0.0, 0.0)
	//rx.DrawSetColor(0.0, 0.0, 0.0)
	// Dark Slate Gray
	//rx.DrawSetColor(0.1843 * 0.2, 0.3098 * 0.2, 0.3098 * 0.2)
	rx.DrawSetColor(0, 0, 0)
	//rx.DrawQuad(0,0,0.33,1.0)
	//r.RenderQuad(0.0, 0, 0.33, 1, 0)
	//r.RenderQuad(0.61, 0, 1, 1, 0)
	r.Set2DMode()
	gl.Disable(gl.DEPTH_TEST) // XXX fixme?
	//rx.DrawLine(0, 0, 0, .5, .5, 0)
	//rx.DrawLine(0, 0, 0, 1, 1, 0)
	//rx.DrawLine(0, 0, 0, 100, 100, 0)
	//rx.DrawQuad(.3,0,0.1, 0.1)
	rx.DrawQuad(0, 0, 0.33, 1)
	rx.DrawQuad(0.61, 0, 1, 1)
	gl.Enable(gl.DEPTH_TEST)
	r.Unset2DMode()

	/*
		r.RenderQuad(0.5, 0.0, 0.2, 0.1, 0)
		r.Set2DMode()
		rx.DrawLine(0, 0, 0, .5, .5, 0)
		//rx.DrawLine(0, 0, 0, 1, 1, 0)
		//rx.DrawLine(0, 0, 0, 100, 100, 0)
		r.Unset2DMode()
	*/
}

func (s *GuiMainSheet) Update(dt float64) {
	// Call base
	s.Sheet.Update(dt)

	//g.topText.SetText(fmt.Sprintf("cellX: %d cellY: %d", g.cellX, g.cellY))
	//s.topText.SetText(fmt.Sprintf("weapon2Power: %.02f lives: %d fps: %d",
	//	game.player.weapon2Power, game.player.lives, _rxi.App.Fps()))
	//s.topText.SetText(fmt.Sprintf("weapon2Power: 1.0 lives: 5 fps: %d", _rxi.App.Fps()))
	//s.topText.SetText(fmt.Sprintf("weapon2Power: 1.0 xxxxx: 5 fps: %d", _rxi.App.Fps()))
	//s.topText.SetText(fmt.Sprintf("weapon2Power: 1.0 xxxxxqoeinfopeinqwuof: 5 oeqwfnpoeqwinf: 10.0 oefwnu: 1.11 fps: %d", _rxi.App.Fps()))
	pb := ""
	iv := int((game.player.weapon2Power * 100) / 20)
	for i := 0; i < iv; i++ {
		//pb += "░"
		//pb += "█"
		//pb += "■"
		pb += "□"
		//pb += "▬"
		//pb += "▱"
	}
	if pb == "" {
		//pb = "█████"
		//pb = "■■■■■"
		//pb = "▲▲▲▲▲"
		//pb = "▬▬▬▬▬"
		//pb = "□□□□□"
	}
	pbLives := ""
	for i := 0; i < game.player.lives; i++ {
		//pbLives += "◇"
		pbLives += "♢"
		//pbLives += "♦"
	}
	//s.topText.SetText(fmt.Sprintf("weapon2Power: %s xxxxx:  fps: %d", pb, _rxi.App.Fps()))
	//s.debugText.SetText(fmt.Sprintf("weapon2Power: |%-5s| |%-10s| fps: %d", pb, pbLives, _rxi.App.Fps()))
	s.debugText.SetText(fmt.Sprintf("|%-5s| |%-10s| %06d", pb, pbLives, game.player.score))
	s.statusText.SetText(fmt.Sprintf("fps: %d", _rxi.App.Fps()))
	//s.topText.SetText(fmt.Sprintf("weapon2Power: %.02f fps: %d",
	//	game.player.weapon2Power, _rxi.App.Fps()))
}
