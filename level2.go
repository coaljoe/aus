package main

import (
	. "bitbucket.org/coaljoe/rx/math"
	//"github.com/go-gl/glfw/v3.1/glfw"
)

type Level2 struct {
	*Level
}

func newLevel2() *Level2 {
	l := &Level2{
		Level: newLevel(),
	}
	l.name = "level2"
	l.fullname = "Level 2"
	//l.musicPath = "res/music/test.ogg"
	//l.musicPath = "res/music/t2a-v2-02.ogg"
	//l.musicPath = "res/music/t2a-v2-02a.ogg"
	//l.musicPath = "res/music/t2a-v3a.ogg"
	l.musicPath = "res/music/Metome - Impro 2016l4l2.ogg"
	return l
}

func (l *Level2) start() {
	l.Level.start()

	//game.gui.showTopText("LEVEL 2")
	l.popupLevelNameText()

	// Background

	//prevConfValue := rx.Conf.DefaultMaterialPath
	//rx.Conf.DefaultMaterialPath = "/materials/model.json"
	bg := game.bg
	bg.addTiles(3)
	bg.tiles[0].placeMeshTimes(10, "res/objects/tree/tree.dae")
	bg.tiles[1].placeMeshTimes(10, "res/objects/tree/tree.dae")
	bg.tiles[2].placeMeshTimes(50, "res/objects/tree/tree.dae")
	//rx.Conf.DefaultMaterialPath = prevConfValue

	// Wave 1
	go func() {
		sleepsecAndCheckGS(5)
		for i := 0; i < 5; i++ {
			go func(iv float64) {
				sleepsecAndCheckGS(iv)
				makeEnemyAt("enemy2", Vec3{4.0 + 8.0*float64(iv), 0, 0})
			}(float64(i))
		}
	}()
}
