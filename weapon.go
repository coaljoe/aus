package main

import (
	. "bitbucket.org/coaljoe/rx/math"
)

// Компонент итема отвечающий за ф-ции оружия.
// component
type Weapon struct {
	// link to host cc
	cc *Combat
	// link to host item
	//item        *Item
	rounds     int
	maxRounds  int
	fireRange  float64 // m
	fireRate   float64 // shots/min
	reloadTime float64 // s
	shotName   string
	shotSpeed  float64
	shotSound  string
	useCounter int
	// state
	firing            bool
	fireIntervalTimer *Timer
}

func newWeapon(cc *Combat) *Weapon {
	w := &Weapon{
		cc: cc,
		//item:              it,
		rounds:            0,
		maxRounds:         10,
		fireRange:         10,
		fireRate:          50,
		reloadTime:        1,
		shotName:          "shot",
		shotSpeed:         100,
		shotSound:         "res/audio/shot2a.wav",
		useCounter:        0,
		fireIntervalTimer: newTimer(false),
	}
	w.reload()
	return w
}

func (w *Weapon) destroy() {
	timersys().removeTimer(w.fireIntervalTimer)
}

func (w *Weapon) fire() {
	_log.Dbg("weapon.fire")

	// make bullet
	view := w.cc.host.getView()
	//pf("XXX: %v\n", view.(*EnemyView))
	if view == nil {
		panic("no view")
	}
	//fpNode := view.getNode("firepointNode")
	fpNode := view.getNode("body")
	//fpNode := view.getNode("fire_point")
	//pf("YYY: %v\n", fpNode)
	//pf("YYY: %#v\n", fpNode)
	//srcPos := fpNode.WorldPos()
	srcPos := fpNode.Pos()
	//p("XXX ", srcPos)
	//p := w.target.Pos2() // fixme
	p := srcPos
	//p.Y += 30
	//p.Y = 30
	//p.SetY(30)
	byEnemy := false
	if w.cc.host.getObjType() == "enemy" {
		byEnemy = true
	}
	var dstPos Vec3
	if !byEnemy {
		p.SetY(gameWindowH)
		dstPos = Vec3{p.X(), p.Y(), p.Z()}
		//srcPos = srcPos.Add(Vec3{0, 3.5, 0}) // Adjust fire point
		srcPos = srcPos.Add(Vec3{0, 5.0, -1.0}) // Adjust fire point
	} else {
		dstPos = Vec3{p.X(), -1, p.Z()}
		srcPos = srcPos.Add(Vec3{0, -3.5, 0}) // Adjust fire point

	}
	//_log.Dbg("shot from:", srcPos, "to:", dstPos, "speed:", w.shotSpeed)
	_log.Inf("shot from:", srcPos, "to:", dstPos, "speed:", w.shotSpeed)

	pr := newShot(w.shotName, srcPos, dstPos, w.shotSpeed, byEnemy)
	spawnEntity(pr)
	if byEnemy {
		// Rotate the shot
		pr.position.dir.setAngle(-90)
	}
	//game.entitysys.listEntities()
	//pp(2)

	// Play sound
	//playSound("res/audio/test.wav")
	playSound(w.shotSound, vol_sfx)
	/*
		if w.shotName == "shot" {
			//playSound("res/audio/slimeball.wav", vol_sfx)
			playSound("res/audio/shot1.wav", vol_sfx)
		} else if w.shotName == "shot2" {
			playSound("res/audio/iceball.wav", vol_sfx)
		} else {
			pp("unknown shotName sound")
		}
	*/

	if w.rounds == 0 {
		if !w.reload() {
			_log.Dbg("no bullets")
			return
		}
	}

	// fire
	w.rounds -= 1
	w.useCounter += 1

	w.fireIntervalTimer.restart()
}

func (w *Weapon) canFire() bool {
	if w.rounds < 0 {
		return false
	}

	// check readiness
	if w.fireIntervalTimer.dt() < w.fireInterval() {
		return false
	}

	return true
}

func (w *Weapon) startFiring() {
	w.firing = true
}

func (w *Weapon) stopFiring() {
	w.firing = false
}

func (w *Weapon) reload() bool {
	_log.Dbg("weapon.reload")
	// todo: use ammo
	w.rounds = w.maxRounds
	return true
}

// fire interval in seconds
func (w *Weapon) fireInterval() float64 {
	return 1.0 / (w.fireRate / 60.0)
}

func (w *Weapon) hasAmmo() bool {
	return w.rounds > 0
}

func (w *Weapon) update(dt float64) {
	// update firing
	if w.firing {
		if w.canFire() {
			w.fire()
		} else {
			if !w.hasAmmo() {
				w.reload()
			}
		}
	}
}
