package main

import (
	. "bitbucket.org/coaljoe/rx/math"
	//"github.com/go-gl/glfw/v3.1/glfw"
)

type Level1 struct {
	*Level
}

func newLevel1() *Level1 {
	l := &Level1{
		Level: newLevel(),
	}
	l.name = "level1"
	l.fullname = "Level 1"
	//l.musicPath = "res/music/test.ogg"
	//l.musicPath = "res/music/t2a-v2-02.ogg"
	//l.musicPath = "res/music/t2a-v2-02a.ogg"
	//l.musicPath = "res/music/t2a-v3a.ogg"
	l.musicPath = "res/music/Metome - Impro 2016l4l2.ogg"
	return l
}

func (l *Level1) start() {
	l.Level.start()
	game.gui.hideTopText() // Fixes the bug

	// Background

	//prevConfValue := rx.Conf.DefaultMaterialPath
	//rx.Conf.DefaultMaterialPath = "/materials/model.json"
	bg := game.bg
	bg.addTiles(3)
	bg.tiles[0].placeMeshTimes(10, "res/objects/tree/tree.dae")
	bg.tiles[1].placeMeshTimes(10, "res/objects/tree/tree.dae")
	bg.tiles[2].placeMeshTimes(50, "res/objects/tree/tree.dae")
	//rx.Conf.DefaultMaterialPath = prevConfValue

	// Wave 1
	wave1 := func() {
		println("callback")
		e1 := makeEnemy("enemy1")
		spawnEntity(e1)
		e2 := makeEnemy("enemy2")
		e2.SetPos(Vec3{10, 0, 0})
		spawnEntity(e2)
		//pp(2)
	}
	l.addEvent(5, wave1)
	_ = wave1

	var _ = `
	cbs := make([]func(), 0)

	for i := 0; i < 5; i++ {
		p("I=", i)
		/*
		l.addEvent(5.0 + 1.0 * float64(i), func() {
			p("POS", i)
			//e := makeEnemy("enemy1")
			//e.SetPos(Vec3{2.0 * float64(i), 0, 0})
			//p("POS", Vec3{2.0 * float64(i), 0, 0}, i)
			//pp(Vec3{2.0 * float64(i), 0, 0}, i)
			//spawnEntity(e)
		})
		*/
		f := func() {
			p("POS", i)
			//pp(2)
		}
		cbs = append(cbs, f)
	}
	for _, cb := range cbs {
		cb()
	}
	`

	// Wave 2
	l.addEvent(5.0+1.0, func() { makeEnemyAt("enemy1", Vec3{2.0, 0, 0}) })
	l.addEvent(5.0+2.0, func() { makeEnemyAt("enemy1", Vec3{4.0, 0, 0}) })
	l.addEvent(5.0+3.0, func() { makeEnemyAt("enemy1", Vec3{6.0, 0, 0}) })
	l.addEvent(5.0+4.0, func() { makeEnemyAt("enemy1", Vec3{8.0, 0, 0}) })
	l.addEvent(5.0+5.0, func() { makeEnemyAt("enemy1", Vec3{10.0, 0, 0}) })

	// Wave 3
	t := 10.0
	l.addEvent(t+1.0, func() { makeEnemyAt("enemy2", Vec3{20.0 * 2, 0, 0}) })
	l.addEvent(t+1.5, func() { makeEnemyAt("enemy2", Vec3{16.0 * 2, 0, 0}) })
	l.addEvent(t+2.0, func() { makeEnemyAt("enemy2", Vec3{12.0 * 2, 0, 0}) })
	l.addEvent(t+2.5, func() { makeEnemyAt("enemy2", Vec3{8.0 * 2, 0, 0}) })
	l.addEvent(t+3.0, func() { makeEnemyAt("enemy2", Vec3{4.0 * 2, 0, 0}) })

	// Wave 4
	t = 13.0
	s := 8.0
	l.addEvent(t+1.0, func() { makeEnemyAt("enemy1", Vec3{s * 0, 0, 0}) })
	l.addEvent(t+1.5, func() { makeEnemyAt("enemy1", Vec3{s * 1, 0, 0}) })
	l.addEvent(t+2.0, func() { makeEnemyAt("enemy1", Vec3{s * 2, 0, 0}) })
	l.addEvent(t+2.5, func() { makeEnemyAt("enemy1", Vec3{s * 3, 0, 0}) })

	l.addEvent(t+3.5, func() { makeEnemyAt("enemy1", Vec3{s * 6, 0, 0}) })
	l.addEvent(t+4.0, func() { makeEnemyAt("enemy1", Vec3{s * 5, 0, 0}) })
	l.addEvent(t+4.5, func() { makeEnemyAt("enemy1", Vec3{s * 4, 0, 0}) })
	l.addEvent(t+5.0, func() { makeEnemyAt("enemy1", Vec3{s * 3, 0, 0}) })

	// Wave 5
	t = 18.0
	s = 8.0
	l.addEvent(t+1.0, func() { makeEnemyAt("enemy1", Vec3{s * 0, 0, 0}) })
	l.addEvent(t+1.5, func() { makeEnemyAt("enemy1", Vec3{s * 1, 0, 0}) })
	l.addEvent(t+2.0, func() { makeEnemyAt("enemy1", Vec3{s * 2, 0, 0}) })
	l.addEvent(t+2.5, func() { makeEnemyAt("enemy1", Vec3{s * 3, 0, 0}) })

	l.addEvent(t+3.5, func() { makeEnemyAt("enemy1", Vec3{s * 6, 0, 0}) })
	l.addEvent(t+4.0, func() { makeEnemyAt("enemy1", Vec3{s * 5, 0, 0}) })
	l.addEvent(t+4.5, func() { makeEnemyAt("enemy1", Vec3{s * 4, 0, 0}) })
	l.addEvent(t+5.0, func() { makeEnemyAt("enemy1", Vec3{s * 3, 0, 0}) })

	// Wave 6
	t = 25.0
	s = 8.0
	l.addEvent(t+1.0, func() { makeEnemyAt("enemy2", Vec3{s * 0, 0, 0}) })
	l.addEvent(t+1.5, func() { makeEnemyAt("enemy2", Vec3{s * 1, 0, 0}) })
	l.addEvent(t+2.0, func() { makeEnemyAt("enemy2", Vec3{s * 2, 0, 0}) })
	l.addEvent(t+2.5, func() { makeEnemyAt("enemy2", Vec3{s * 3, 0, 0}) })

	l.addEvent(t+3.5, func() { makeEnemyAt("enemy2", Vec3{s * 6, 0, 0}) })
	l.addEvent(t+4.0, func() { makeEnemyAt("enemy2", Vec3{s * 5, 0, 0}) })
	l.addEvent(t+4.5, func() { makeEnemyAt("enemy2", Vec3{s * 4, 0, 0}) })
	l.addEvent(t+5.0, func() { makeEnemyAt("enemy2", Vec3{s * 3, 0, 0}) })

	// Wave 7
	t = 30.0
	s = 10.0
	l.addEvent(t+1.0, func() { makeEnemyAt("enemy1", Vec3{s * 0, 0, 0}) })
	l.addEvent(t+1.2, func() { makeEnemyAt("enemy1", Vec3{s * 1, 0, 0}) })
	l.addEvent(t+2.0, func() { makeEnemyAt("enemy1", Vec3{s * 2, 0, 0}) })
	l.addEvent(t+2.2, func() { makeEnemyAt("enemy1", Vec3{s * 3, 0, 0}) })

	l.addEvent(t+4.0, func() { makeEnemyAt("enemy1", Vec3{s * 6, 0, 0}) })
	l.addEvent(t+4.2, func() { makeEnemyAt("enemy1", Vec3{s * 5, 0, 0}) })
	l.addEvent(t+5.0, func() { makeEnemyAt("enemy1", Vec3{s * 4, 0, 0}) })
	l.addEvent(t+5.2, func() { makeEnemyAt("enemy1", Vec3{s * 3, 0, 0}) })

	// Wave 8
	go func() {
		//sleepsec(36)
		//checkGS()
		sleepsecAndCheckGS(36)
		for i := 0; i < 5; i++ {
			p("I=", i)
			go func(iv float64) {
				//sleepsec(float64(i))
				//p("sleepsec:", float64(i))
				//sleepsec(iv)
				//checkGS()
				sleepsecAndCheckGS(iv)
				p("sleepsec:", iv)
				/*
					e := makeEnemy("enemy1")
					e.SetPos(Vec3{8.0 * float64(iv), 0, 0})
					spawnEntity(e)
				*/
				makeEnemyAt("enemy1", Vec3{8.0 * float64(iv), 0, 0})
			}(float64(i))
		}
	}()

	// Go to level2
	go func() {
		sleepsecAndCheckGS(45)
		game.finishLevel()
		game.playLevel("level2")
	}()

	/*
		// Finish level
		l.addEvent(10, func() {
			l.finish()
			game.playLevel("level2")
		})
	*/

	/*
		go func() {
			sleepsec(100)
			pp(2)
		}()
	*/
	/*
		go func() {
			sleepsec(5)
			println("callback")
			rx.Rxi().App.MakeContextCurrent()
			win := glfw.GetCurrentContext()
			win.MakeContextCurrent()
			e1 := makeEnemyEntity("enemy1")
			spawnEntity(e1)
			e2 := makeEnemyEntity("enemy2")
			e2.SetPos(Vec3{10, 0, 0})
			spawnEntity(e2)
		}()
	*/
	/*
		go func() {
				sleepsec(5)
				println("callback")
				//rx.Rxi().App.MakeContextCurrent()
				//win := glfw.GetCurrentContext()
				//win.MakeContextCurrent()
				//swin := rx.Rxi().App.Win().GlfwWinSlave()
				//swin.MakeContextCurrent()
				e1 := makeEnemy("enemy1")
				spawnEntity(e1)
				e2 := makeEnemy("enemy2")
				e2.SetPos(Vec3{10, 0, 0})
				spawnEntity(e2)
			}()
	*/

}
