package main

import (
	"strings"
)

/*
type LevelEvent struct {
	delay float64
}
*/

type LevelI interface {
	start()
	finish()
}

type Level struct {
	name      string
	fullname  string
	musicPath string
	//events []*LevelEvent
	events []*Shed
}

func newLevel() *Level {
	l := &Level{}
	return l
}

func (l *Level) start() {
	if l.musicPath != "" {
		playMusic(l.musicPath)
	}
}

func (l *Level) finish() {
	// Clear the scene
	//game.clearGame()

	stopMusic()
}

func (l *Level) addEvent(dtime float64, cb func()) {
	_ = newShed(dtime, cb)
}

func (l *Level) popupLevelNameText() {
	s := strings.ToUpper(l.fullname)
	game.gui.popupTopText(s, 2)
}

func (l *Level) update(dt float64) {

}
