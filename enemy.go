package main

import (
	. "bitbucket.org/coaljoe/rx/math"
)

// entity
type Enemy struct {
	*Obj
	health    *Health
	combat    *Combat
	position  *Position
	movingBhv MovingBhvI
	// props
	name      string
	color     Vec3
	killScore int
}

func newEnemy() *Enemy {
	e := &Enemy{
		Obj:       newObj("enemy"),
		health:    newHealth(),
		color:     Vec3Zero,
		killScore: 100,
	}
	e.colobj = newColObj(e.Obj, 5)
	e.combat = newCombat(e, e.health)
	e.position = newPosition(e.Obj)
	e.movingBhv = newStandardMovingBhv(e)
	//e.movingBhv = newStrafingMovingBhv(e)
	e.view = newEnemyView(e)

	// Tune
	e.combat.weapon1.fireRate = 35
	//e.combat.weapon1.shotSound = "res/audio/shot1.wav"
	//e.combat.weapon1.shotName = "shot3"
	return e
}

func (c *Enemy) start() {
	c.AdjPos(Vec3{0, gameWindowH, 0})
	/*
		c.position.dir.setAngle(-90)
		dPos := Vec3{c.Pos().X(), -10, 0}
		c.position.setDPos(dPos)
		//c.position.setDPos(Vec3Zero)
		c.position.speed = c.moveSpeed
		//c.position.speed = 1
	*/
	c.movingBhv.start()
	//if c.getId() == 1 {
	c.combat.weapon1.rounds = 10
	c.combat.weapon1.fireIntervalTimer.randomize(c.combat.weapon1.fireInterval())
	//p(c.combat.weapon1.fireInterval())
	//p(c.combat.weapon1.fireIntervalTimer.time)
	c.combat.weapon1.startFiring()
	/*
		cb := func(){
			c.combat.weapon1.startFiring()
		}
		_ = newShed(1, cb)
	*/
	//}
}

func (c *Enemy) destroy() {
	c.Obj.destroy()
	c.health.destroy()
	c.combat.destroy()
	c.position.destroy()
}

func (e *Enemy) explode() {
	p("Enemy.explode")
	playSound("res/audio/SFX_Explosion_03.wav", vol_sfx)
	destroyEntity(e)
	game.player.score += e.killScore
	//e.destroy()
	//pp(2)
}

func (c *Enemy) update(dt float64) {

	/*
		curPos := c.Pos()
		newPos := curPos.Add(c.moveAdj.MulScalar(c.moveSpeed * dt))
		c.SetPos(newPos)
	*/

	c.Obj.update(dt)
	c.position.update(dt)
	c.combat.update(dt)
	c.movingBhv.update(dt)

	if c.Pos().Y() <= -10 {
		//c.destroy()
		destroyEntity(c)
	}
}
