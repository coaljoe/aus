package main

///////////////////////////////////////
// define objects
var (
	maxObjId = -1
)

// Any object in the world.
type Obj struct {
	id   int
	name string
}

type ObjI interface {
	Id() int
	Name() string
	update(dt float64)
}

func (ob *Obj) Id() int      { return ob.id }
func (ob *Obj) Name() string { return ob.name }

func newObj() *Obj {
	maxObjId += 1
	return &Obj{id: maxObjId, name: ""}
}

//-------------------------------------

// Common unit.
type Unit struct {
	maxSpeed int
}

func newGroundUnit() *Unit {
	return &Unit{maxSpeed: 10}
}

func newNavyUnit() *Unit {
	return &Unit{maxSpeed: 3}
}

func newAirUnit() *Unit {
	return &Unit{maxSpeed: 600}
}

func (u *Unit) update(dt float64) {
	println("Unit.update")
}

//-------------------------------------

// Common turret
type Turret struct {
	rotAngle float64
	name     string
}

func newLightTurret() *Turret {
	return &Turret{
		rotAngle: 180,
		name:     "lightturret",
	}
}

func (t Turret) fire() {
	println("turret " + t.name + " firing")
}

func (t *Turret) update(dt float64) {
	println("Turret.update")
}

//-------------------------------------

// Common tank.
type Tank struct {
	*Obj
	*Unit
	turret *Turret
	color string
}

func newLightTank() *Tank {
	return &Tank{
		Obj:    newObj(),
		Unit:   newGroundUnit(),
		turret: newLightTurret(),
		color: "green",
	}
}

func newRedTank() *Tank {
	t := newLightTank()
	t.color = "red"
	return t
}

func (t *Tank) fire() {
	t.turret.fire()
}

func (t *Tank) update(dt float64) {
	println("Tank.update")
	t.Unit.update(dt)
	t.turret.update(dt)
}

//-------------------------------------

/*
type LightTank struct {
	*Unit
	*Tank
}
*/

func unitFactory(typename string) ObjI {
	switch typename {
	case "lighttank":
		u := newLightTank()
		return u
	}
	panic(2)
}

func main() {
	//u1 := tankFactory("lighttank")
	u1 := unitFactory("lighttank")
	println(u1.Id())
	u1.(*Tank).fire() // downcast?
	u1.update(1)
	println(u1.(*Tank).color) // downcast?

	u2 := newRedTank()
	u2.update(1)
	println(u2.color)
	println(u2.maxSpeed)
}
