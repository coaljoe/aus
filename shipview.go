package main

import (
	"bitbucket.org/coaljoe/rx"
	//. "bitbucket.org/coaljoe/rx/math"
)

type ShipView struct {
	*View
	m    *Ship
	node *rx.Node
}

func newShipView(m *Ship) *ShipView {
	v := &ShipView{
		View: newView(),
		m:    m,
	}
	return v
}

func (v *ShipView) load() {
	//path := "res/enemies/enemy1/enemy1.dae"
	path := "res/ship/ship.dae"
	v.model.Load(path)
	v.node = v.model.GetNode("body")
	if v.node == nil {
		panic("no v.node")
	}
	v.loaded = true
}

func (v *ShipView) spawn() {
	if !v.loaded {
		v.load()
	}
	//v.node.SetRot(Vec3{90, 0, -90})
	//v.node.SetRot(Vec3{0, 0, 90})
	rx.Rxi().Scene().Add(v.node)
	//v.spawned = true
	//pp(v.node.Material().Diffuse)
}

func (v *ShipView) destroy() {
	rx.Rxi().Scene().RemoveMeshNode(v.node)
}

func (v *ShipView) hide() {
	v.node.SetVisible(false)
}

func (v *ShipView) show() {
	v.node.SetVisible(true)
}

func (v *ShipView) isVisible() bool {
	return v.node.Visible()
}

func (v *ShipView) update(dt float64) {
	v.node.SetPos(v.m.Pos())
	v.node.SetRot(v.m.Rot())
	//v.node.SetRot(Vec3{0, 0, 90})
}
