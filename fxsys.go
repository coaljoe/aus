package main

type FxSys struct {
	*GameSystem
}

func newFxSys() *FxSys {
	s := &FxSys{}
	s.GameSystem = newGameSystem("FxSys", "Fx system", s)
	return s
}

func (s *FxSys) update(dt float64) {
	// Ship blinking
	ship := game.ship
	if ship.immortal {
		shipView := ship.getView().(*ShipView)
		if shipView.isVisible() {
			shipView.hide()
		} else {
			shipView.show()
		}
	}
}
