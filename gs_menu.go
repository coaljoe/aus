package main

import (
	"github.com/go-gl/glfw/v3.1/glfw"
	ps "bitbucket.org/coaljoe/lib/pubsub"
	"bitbucket.org/coaljoe/rx"
)

type MenuGameState struct {
	*GameState
}

func newMenuGameState() *MenuGameState {
	gs := &MenuGameState{
		GameState: newGameState("menu"),
	}
	return gs
}

func (gs *MenuGameState) onKeyPress(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	//pp(key)
	if key == glfw.KeyEnter {
		setGameState("game")
		//setGameState("game over")
	} else if key == glfw.KeyEscape {
		rx.Rxi().App.SetShouldClose(true)
	}
}

func (gs *MenuGameState) start() {
	g := game.gui
	g.rgi.SheetSys.SetActiveSheet(g.menuSheet)

	/*
		_Keymap.addKey(KeymapRec{Name: "enterKey", Key1: glfw.KeyEnter,
			Description:     "Enter key",
			DescriptionLong: ""})
	*/
	playMusic("res/music/t2a-v3a.ogg")
	sub(rx.Ev_key_press, gs.onKeyPress)
}

func (gs *MenuGameState) stop() {
	//g := game.gui
	//g.rgi.SheetSys.SetActiveSheet(g.mainSheet)

	//_Keymap.removeKey("enterKey")
	stopMusic()
	unsub(rx.Ev_key_press, gs.onKeyPress)
}

func (gs *MenuGameState) update(dt float64) {

}
