package main

import (
	. "bitbucket.org/coaljoe/rx/math"
)

// entity
type Shot struct {
	*Obj
	position  *Position
	ptype     string // Shell, burst, rocket, bomb, custom?
	sPos      Vec3
	dPos      Vec3
	moveSpeed float64
	//pathCurveness float64
	//ammotype      *AmmoType
	//emitter       *ecs.Entity // Link to emitter entity
	//target        *ecs.Entity // Link to target
	//moveStartTime  float64
	// props
	name string
}

func newShot(shotName string, sPos, dPos Vec3, speed float64, byEnemy bool) *Shot {

	p := &Shot{
		//Obj:           newObj(en, "projectile obj"),
		//ptype:         ammotype.projectileType,
		sPos:      sPos,
		dPos:      dPos,
		moveSpeed: speed,
		//pathCurveness: pathCurveness,
		//ammotype:      ammotype,
		//emitter:       emitter,
		//target:        target,
		name: shotName,
	}
	if byEnemy {
		p.Obj = newObj("enemy-shot")
	} else {
		p.Obj = newObj("shot")
	}
	p.colobj = newColObj(p.Obj, 0.5)
	p.position = newPosition(p.Obj)
	p.view = newShotView(p)
	//p.SetPos(sPos)
	return p
}

func (p *Shot) explode() {
	destroyEntity(p)
}

func (p *Shot) destroy() {
	p.Obj.destroy()
	p.position.destroy()
}

func (p *Shot) start() {
	//p.position.dir.setAngle(-90)
	p.position.dir.setAngle(90)
	p.SetPos(p.sPos)
	// Go
	p.position.setDPos(p.dPos)
	p.position.speed = p.moveSpeed
}

func (p *Shot) update(dt float64) {

	/*
		// Finised travel
		if !p.isMoving() {
			//p.delete() // XXX call explicit?
			//p.View.destroy()
			pub(ev_projectile_hit,
				EvShotHit{
					pos: p.Pos(), ammotype: p.ammotype, emitter: p.emitter,
					target: p.target,
				})
			println("deleting projectile, eid=", GetEntity(p).Id())
			//deleteEntity(GetEntity(p))
			deleteShot(GetEntity(p))
			//pp(2)
			return
		}
	*/

	/*
		if b.moveStartTime == 0 {
			b.moveStartTime = _now()
		}

			// Total time of travel A -> B [constant]
			tt := distancePos2(Vec2{b.sPos.X(), b.sPos.Y()},
				Vec2{b.dPos.X(), b.dPos.Y()}) / b.moveSpeed
			t := (_now() - b.moveStartTime) / tt
			nextPos := Lerp3(b.sPos, b.dPos, t)

			if roundPrec(t, 2) < 1.0 {
				b.SetPos(nextPos)
			} else {
				b.delete()
				b.View.destroy()
				pub(ev_bullet_hit, b)
				return
			}
	*/

	// auto destroy projectiles
	if p.Pos().Y() >= gameWindowH || p.Pos().Y() <= -1.0 {
		//c.destroy()
		//destroyEntity(p)
		p.dead = true
	}

	p.position.update(dt)
}

/*
func makeShot(sPos, dPos Vec3, speed,
	pathCurveness float64, ammotype *AmmoType, emitter *ecs.Entity,
	target *ecs.Entity) *ecs.Entity {

	en := newEntity()
	_ = newShot(en, sPos, dPos, speed, pathCurveness, ammotype,
		emitter, target)

	// Add to fx system
	game.getSystem("FxSys").addElem(en)
	return en
}
*/

/*
func deleteShot(en *ecs.Entity) {
	//game.getSystem("FxSys").removeElem(en)
	//p := en.Get(ShotT).(*Shot)
	//p.delete()
	deleteEntity(en)
}

func makeShotView(en *ecs.Entity, p *Shot) *ShotView {
	if p.ptype == "shell" || p.ptype == "rocket" {
		v := newShotView(en, p)
		return v
	} else {
		panic("unknown ptype: " + p.ptype)
	}
}
*/
