module gitlab.com/coaljoe/aus

// go: no requirements found in Godeps/Godeps.json

require (
	bitbucket.org/coaljoe/lib v0.0.0-20190220232856-c944304543fd
	bitbucket.org/coaljoe/rx v0.0.0-20190228010900-88218f6cb242
	github.com/bitly/go-simplejson v0.5.0
	github.com/davecgh/go-spew v1.1.1
	github.com/flynn/json5 v0.0.0-20160717195620-7620272ed633
	github.com/go-gl/gl v0.0.0-20181026044259-55b76b7df9d2
	github.com/go-gl/glfw v0.0.0-20190217072633-93b30450e032
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/veandco/go-sdl2 v0.3.0
)

replace bitbucket.org/coaljoe/rx => ../rx_orig
