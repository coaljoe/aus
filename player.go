package main

import (
	"fmt"
)

type Player struct {
	id       int
	ptype    PlayerType
	name     string
	lives    int
	maxLives int
	score    int
	// Weapon 2 power level
	weapon2Power            float64
	weapon2PowerChargeSpeed float64
	weapon2PowerShootCost   float64
	weapon2BeepLock         bool
	//ai    *PlayerAi
	// Stats
	//militaryScore    int
	//statsUpdateTime  float64
	//statsUpdateTimer *Timer
	//vc       VarsCache
	disabled bool
}

func newPlayer(ptype PlayerType) *Player {
	p := &Player{
		id:                      _ider.GetNextId("Player"),
		ptype:                   ptype,
		name:                    "Unknown",
		lives:                   5,
		maxLives:                10,
		weapon2Power:            1.0,
		weapon2PowerChargeSpeed: 0.35,
		weapon2PowerShootCost:   0.035,
		//statsUpdateTime:  2, //10,
		//statsUpdateTimer: newTimer(true),
		//vc:       newVarsCache(),
		disabled: false,
	}
	if ptype == PlayerType_AI {
		//p.ai = newPlayerAi(p)
	}

	// Automatically add to PlayerSys
	//game.playersys.addPlayer(p) // Fixme: remove?
	return p
}

func (p *Player) isHuman() bool {
	if p.ptype == PlayerType_Human {
		return true
	}
	return false
}

func (p *Player) isAi() bool {
	return !p.isHuman()
}

func (p *Player) getScore() int {
	return 0
}

func (p *Player) takeLives() bool {
	p.lives -= 1
	if p.lives < 0 {
		p.lives = 0
		return false
	}
	return true
}

func (p *Player) addLives(amt int) {
	p.lives += amt
	if p.lives > p.maxLives {
		p.lives = p.maxLives
	}
}

func (p *Player) takeWeapon2Power() bool {
	p.weapon2Power -= p.weapon2PowerShootCost
	if p.weapon2Power < 0 {
		p.weapon2Power = 0
		return false
	}
	return true
}

/*
func (p *Player) _updateStats() {
	ms := p.getMilitaryScore()
	p.militaryScore = ms
}
*/

func (p *Player) String() string {
	return fmt.Sprintf("Player<name: %s>", p.name)
}

func (p *Player) update(dt float64) {
	if p.disabled {
		return
	}

	if p.weapon2Power < 1.0 {
		p.weapon2Power += p.weapon2PowerChargeSpeed * dt
		if p.weapon2Power > 1.0 {
			p.weapon2Power = 1.0
		}
	}
	//pp(pai.statsUpdateTimer.dt())
	/*
		if p.statsUpdateTimer.dt() > p.statsUpdateTime {
			p._updateStats()
			p.statsUpdateTimer.restart()
		}
	*/
}

///////////////
// PlayerType

type PlayerType int

const (
	PlayerType_Human PlayerType = iota
	PlayerType_AI
)

func (p PlayerType) String() string {
	switch p {
	case PlayerType_Human:
		return "Human"
	case PlayerType_AI:
		return "AI"
	default:
		panic("unknown PlayerType")
	}
}
